<?php

namespace MGVmedia;

class Street {
  private $db;
  private $id = 0;
  private $where = array(" 1=1 ");

  public static function create() {
    return new self();
  }

  public function __construct() {
    $this->db = new Database(Config::$db_hostname, Config::$db_database, Config::$db_username, Config::$db_password);
    $this->db->enableLogging($_SERVER['REMOTE_ADDR'], "mv-street-compare-logs");
  }

  public function id($id) {
    $this->id = $id;
    return $this;
  }

  public function setStatus($status) {
    return $this->db->updateRow("mv-street-compare-streets", $this->id, array(
      "status" => $status
    ));
  }


  public function bbox($bbox) {
    list($left, $bottom, $right, $top) = $bbox;

    $this->where[] = "
    (
      (
        -- Bottom, Left
        `left` <= '".$this->db->escape($left)."' AND
        '".$this->db->escape($left)."' <= `right` AND
        `right` <= '".$this->db->escape($right)."' AND
        `bottom` <= '".$this->db->escape($bottom)."' AND
        '".$this->db->escape($bottom)."' <= `top` AND
        `top` <= '".$this->db->escape($top)."'
      ) OR (
        -- Top, Left
        `left` <= '".$this->db->escape($left)."' AND
        '".$this->db->escape($left)."' <= `right` AND
        `right` <= '".$this->db->escape($right)."' AND
        `bottom` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($top)."' <= `top` AND
        '".$this->db->escape($bottom)."' <= `bottom`
      ) OR (
        -- Bottom, Right
        `left` <= '".$this->db->escape($right)."' AND
        '".$this->db->escape($right)."' <= `right` AND
        '".$this->db->escape($left)."' <= `left` AND
        `bottom` <= '".$this->db->escape($bottom)."' AND
        '".$this->db->escape($bottom)."' <= `top` AND
        `top` <= '".$this->db->escape($top)."'
      ) OR (
        -- Top, Right
        `left` <= '".$this->db->escape($right)."' AND
        '".$this->db->escape($right)."' <= `right` AND
        '".$this->db->escape($left)."' <= `left` AND
        `bottom` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($top)."' <= `top` AND
        '".$this->db->escape($bottom)."' <= `bottom`
      ) OR (
        -- Inside
        '".$this->db->escape($left)."' <= `left` AND
        `right` <= '".$this->db->escape($right)."' AND
        '".$this->db->escape($bottom)."' <= `bottom` AND
        `top` <= '".$this->db->escape($top)."'
      ) OR (
        -- Left
        `left` <= '".$this->db->escape($left)."' AND
        `right` <= '".$this->db->escape($right)."' AND
        '".$this->db->escape($left)."' <= `right` AND
        `top` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($bottom)."' <= `bottom`
      ) OR (
        -- Right
        '".$this->db->escape($left)."' <= `left` AND
        '".$this->db->escape($right)."' <= `right` AND
        `left` <= '".$this->db->escape($right)."' AND
        `top` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($bottom)."' <= `bottom`
      ) OR (
        -- Top
        '".$this->db->escape($left)."' <= `left` AND
        `right` <= '".$this->db->escape($right)."' AND
        '".$this->db->escape($top)."' <= `top` AND
        `bottom` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($bottom)."' <= `bottom`
      ) OR (
        -- Bottom
        '".$this->db->escape($left)."' <= `left` AND
        `right` <= '".$this->db->escape($right)."' AND
        `bottom` <= '".$this->db->escape($bottom)."' AND
        `top` <= '".$this->db->escape($top)."' AND
        '".$this->db->escape($bottom)."' <= `top`
      )
    )
    ";
    return $this;
  }

  public function status($status) {
    $this->where[] = "
    `status` = '".$this->db->escape($status)."'
    ";

    return $this;
  }

  public function hint($hint) {
    $this->where[] = "
    `hint` = '".$this->db->escape($hint)."'
    ";

    return $this;
  }

  public function osmId($osmid) {
    $this->where[] = "
    `osmid` = '".$this->db->escape($osmid)."'
    ";

    return $this;
  }

  public function runAll() {
    return $this->db->getRows("
      SELECT
          `id`, `osmid`, `name`, `de:amtlicher_gemeindeschluessel`,
          `gemeindename`, `ref`, `type`, `status`, `hint`,
          `left`,`bottom`,`right`,`top`
      FROM `mv-street-compare-streets`
      WHERE ".implode(" AND ", $this->where)."
      ORDER BY `name`
    ");

  }

  public function run() {
    return $this->db->getRows("
      SELECT
          `id`, `osmid`, `name`, `de:amtlicher_gemeindeschluessel`,
          `gemeindename`, `ref`, `type`, `status`, `hint`,
          `left`,`bottom`,`right`,`top`,
          ABS((`right` - `left`) * (`top` - `bottom`)) AS `size`
      FROM (
        (
          SELECT *
          FROM `mv-street-compare-streets`
          WHERE `hint` = 'missing_name'
          GROUP BY `osmid`
        )
        UNION
        (
          SELECT *
          FROM `mv-street-compare-streets`
          WHERE `hint` = 'bad_name'
        )
      ) `i`
      WHERE ".implode(" AND ", $this->where)."
      ORDER BY `size` DESC
      LIMIT 15
    ");
  }


  public static function checkBbox($bbox) {
    if (preg_match('|^(\d+(\.\d+)?),(\d+(\.\d+)?),(\d+(\.\d+)?),(\d+(\.\d+)?)$|', $bbox, $result)) {
      $left = $result[1];
      $bottom = $result[3];
      $right = $result[5];
      $top = $result[7];

      if ($left > $right || $right < $left || $bottom > $top || $top < $bottom) {
        throw new \Exception("No valid BBox given");
      }

      return array(
        $left,
        $bottom,
        $right,
        $top
      );
    } else {
      throw new \Exception("No valid BBox given");
    }
  }

  public static function isStatus($status) {
    return in_array($status, array('changed', 'todo', 'wont-fix'));
  }

  public static function isHint($hint) {
    return in_array($hint, array('missing_name', 'bad_name'));
  }
}
