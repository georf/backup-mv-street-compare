$(function() {
  var EditHandler = new function () {
    var edit = $('#edit');
    var content = $('.content', edit).click(function () {
      return false;
    });
    $('#edit-close, #edit').click(function () {
      edit.hide();
    });

    var change = function (id, type) {
      $.post(apiUrl + "street/" + id, { status: type }, function () {
        ResultHandler.clear();
        globalStreets = [];
        reloadStreets();
      });
      edit.hide();
    };

    this.select = function (street) {
      content.html(street.getEditHtml());
      $('.load-josm', content).click(function () {
        $.get($(this).attr('href'), console.log);
      });
      $('.set-changed', content).attr('title', 'Als changed markieren');
      $('.set-todo', content).attr('title', 'Als todo markieren');
      $('.set-wont-fix', content).attr('title', 'Als wont-fix markieren');
      $('.icon').click(function () {
        var icon = $(this);
        var type = '';
        if (icon.hasClass('set-changed')) type = 'changed';
        if (icon.hasClass('set-todo')) type = 'todo';
        if (icon.hasClass('set-wont-fix')) type = 'wont-fix';

        var result = confirm('Auf ' + type + ' setzen?');
        if (result === true) {
          change(icon.closest('tr').attr('data-id'), type);
        }
      });
      edit.show();
    };
  };

  var ResultHandler = new function () {
    var stack = [];
    var result = $('#result');

    var draw = function (street) {
      result.html(street.getDescription());
    };
    this.redraw = function () {
      if (stack.length > 0) {
        draw(stack[stack.length - 1]);
      } else {
        result.html('');
      }
    };
    this.push = function (street) {
      stack.push(street);
      this.redraw();
    };

    this.pop = function () {
      stack.pop();
      this.redraw();
    };

    this.clear = function () {
      stack = [];
      this.redraw();
    };
  };

  var Street = function (properties) {
    var street = this;
    var mouseOver = false;
    var isBad = (properties.hint == 'bad_name');
    this.area = Math.abs((properties.right - properties.left) * (properties.top - properties.bottom));
    var streetOverlay = null;
    var missingStreets = [];
    var missingStreetsLoaded = false;

    var getOverpassApiUrl = function () {
      return 'http://overpass-api.de/api/interpreter?data=[out:json];(way(' + properties.osmid + ');node(w));out;';
    };

    var getMissingStreetsUrl = function () {
      var url = apiUrl + "streets/missing-name/" + properties.osmid

      if ($('#control-changed').is(':checked')) {
        url += "/changed";
      }
      if ($('#control-wont-fix').is(':checked')) {
        url += "/wont-fix";
      }
      if ($('#control-todo').is(':checked')) {
        url += "/todo";
      }
      return url;
    };

    var loadMissingStreets = function () {
      $.get(getMissingStreetsUrl(), function(result) {
        handleApiResult(result, function (streets) {
          missingStreets = streets;
          missingStreetsLoaded = true;
          ResultHandler.redraw();
        });
      }, 'json');
    };

    var loadStreet = function () {
      if (streetOverlay === false) return false;
      if (streetOverlay !== null) return true;

      $.get(getOverpassApiUrl(), function (result) {
        if (result.elements && result.elements.length) {
          var nodes = {};
          var way = null;
          var elem;
          for (var i = 0; i < result.elements.length; i++) {
            elem = result.elements[i];
            if (elem.type == 'way') {
              way = elem;
            } else {
              nodes[elem.id] = L.latLng(elem.lat, elem.lon);
            }
          }
          var latLngs = [];
          for (i = 0; i < way.nodes.length; i ++) {
            latLngs.push(nodes[way.nodes[i]]);
          }
          streetOverlay = L.polyline(latLngs);
          showStreet();
        }
      }, 'json');
    };

    this.getDescription = function () {
      var html = '';
      if (isBad) {
        html += '<h3>Bad:</h3>';
        html += '<ul>';
        html += '<li>' + properties.name + '</li>';
        html += '</ul>';
      } else {
        if (!missingStreetsLoaded) loadMissingStreets();
        html += '<h3>Missing:</h3>';
        html += '<ul>';
        for (var i = 0; i < missingStreets.length; i++) {
          html += '<li>' + missingStreets[i].name + '</li>';
        }
        html += '</ul>';
      }
      return html;
    };

    this.getEditHtml = function () {
      var html = '';
      if (isBad) {
        html += '<h3p>Bad:</p>';
        html += '<h3>' + properties.name + '</h3>';
        html += '<table>';
        html += '<tr>';
        html += '<td>Gemeindename</td>';
        html += '<td colspan="3">' + properties.gemeindename + '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td>Gemeindeschlüssel</td>';
        html += '<td colspan="3">' + properties["de:amtlicher_gemeindeschluessel"] + '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td>Ref</td>';
        html += '<td colspan="3">' + properties.ref + '</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td></td>';
        html += '<td colspan="2"><a class="load-josm" href="http://127.0.0.1:8111/load_and_zoom?left=' + properties.left
          + '&right=' + properties.right
          + '&top=' + properties.top
          + '&bottom=' + properties.bottom
          + '&select=way' + properties.osmid +'">JOSM Bearbeiten</a></td>';
        html += '<td><a href="http://www.openstreetmap.org/browse/way/' + properties.osmid +'">OSM</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<td colspan="4">&nbsp;</td>';
        html += '</tr>';
        html += '<tr data-id="' + properties.id + '">';
        html += '<td>&nbsp;</td>';
        html += '<td><div class="icon set-changed"></div></td>';
        html += '<td><div class="icon set-wont-fix"></div></td>';
        html += '<td><div class="icon set-todo"></div></td>';
        html += '</tr>';
        html += '</table>';
      } else {
        if (!missingStreetsLoaded) loadMissingStreets();
        html += '<h3>Missing:</h3>';
        html += '<table>';
        for (var i = 0; i < missingStreets.length; i++) {
          html += '<tr data-id="' + missingStreets[i].id + '" class="';
          html += (i%2==0)? 'odd' : 'even';
          html += '">';
          html += '<td>' + missingStreets[i].name + '</td>';
          html += '<td><div class="icon set-changed"></div></td>';
          html += '<td><div class="icon set-wont-fix"></div></td>';
          html += '<td><div class="icon set-todo"></div></td>';
          html += '</tr>';
        }
        html += '</table>';
        html += '<p><a class="load-josm" href="http://127.0.0.1:8111/zoom?left=' + properties.left
          + '&right=' + properties.right
          + '&top=' + properties.top
          + '&bottom=' + properties.bottom
          + '">Zoom in JOSM</a></p>';
      }
      return html;
    };

    var showStreet = function () {
      if (!isBad) {
        polygon.setStyle({fillOpacity: 0.1});
      } else {
        if (loadStreet() && mouseOver) {
          map.addLayer(streetOverlay);
        }
      }
    };

    var hideStreet = function () {
      if (!isBad) {
        polygon.setStyle({fillOpacity: 0});
      } else {
        if (streetOverlay) {
          map.removeLayer(streetOverlay);
        }
      }
    };

    var polygon = null;
    this.getPolygon = function () {
      if (!polygon) {
        polygon = L.polygon([
          L.latLng(properties.bottom, properties.left),
          L.latLng(properties.top, properties.left),
          L.latLng(properties.top, properties.right),
          L.latLng(properties.bottom, properties.right)
        ], {
          fillOpacity: 0,
          weight: 2
        });

        polygon.on('mouseover', function () {
          mouseOver = true;
          ResultHandler.push(street);
          showStreet();
        });
        polygon.on('mouseout', function () {
          mouseOver = false;
          ResultHandler.pop();
          hideStreet();
        });
        polygon.on('click', function () {
          EditHandler.select(street);
        });
      }
      return polygon;
    };
  };

  var globalStreets = [];

  var apiUrl = "http://osm.georf.de/MV-Street-Compare/api/";
  var streetLayer = L.layerGroup([]);

  var handleApiResult = function (result, callback) {
    if (result.status == 200) callback.call(null, result.data);
    else console.log(result.message);
  };

  var getReloadStreetUrl = function () {
    var addition = "";
    if ($('#control-changed').is(':checked')) {
      addition += "/changed";
    }
    if ($('#control-wont-fix').is(':checked')) {
      addition += "/wont-fix";
    }
    if ($('#control-todo').is(':checked')) {
      addition += "/todo";
    }
    return map.getBounds().toBBoxString() + addition;
  };

  var reloadStreets = function () {
    loadStreets(getReloadStreetUrl(), function (streets) {
      ResultHandler.clear();
      map.removeLayer(streetLayer);
      streetLayer = L.layerGroup([]);

      var streetsToSort = [];
      $.each(streets, function (i, street) {
        if (!globalStreets[street.id]) globalStreets[street.id] = new Street(street);
        streetsToSort.push(globalStreets[street.id]);
      });
      streetsToSort.sort(function (a, b) {
        var diff = a.area - b.area;
        return (diff == 0)? 0 : diff/Math.abs(diff);
      });
      for (var i = streetsToSort.length - 1; i >= 0; i--) {
        streetLayer.addLayer(streetsToSort[i].getPolygon());
      }
      map.addLayer(streetLayer);
    });
  };

  var loadStreets = function (boundsAsString, callback) {
    $.get(apiUrl + "streets/" + boundsAsString, function(result) {
      handleApiResult(result, callback);
    }, 'json');
  };

  var map = L.map('map').setView([53.69, 12.2], 8);
  map.addLayer(L.tileLayer('http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'));

  map.on('moveend', reloadStreets);

  $('.control').change(reloadStreets);
  reloadStreets();
});
