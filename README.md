MV-Street-Compare
=================

OSM - Straßenvergleich für Mecklenburg-Vorpommern

###⇒ [Ausführliche Beschreibung](https://wiki.openstreetmap.org/wiki/User:Georf/MV-Street-Compare) ⇐

##Verwendete Technologien:
* [Php-Rest-Service](https://github.com/marcj/php-rest-service/)

##Hinweise zur Entwicklung:
* [Live Testen von Regulären Ausdrücken](http://www.regexpad.com/)
