-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 08. November 2013 um 19:49
-- Server Version: 5.1.70
-- PHP-Version: 5.3.2-1ubuntu4.20

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Datenbank: `projects_osm`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mv-street-compare-logs`
--

CREATE TABLE IF NOT EXISTS `mv-street-compare-logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `table` tinytext NOT NULL,
  `table_id` int(10) unsigned NOT NULL,
  `set` text NOT NULL,
  `logged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mv-street-compare-streets`
--

CREATE TABLE IF NOT EXISTS `mv-street-compare-streets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `osmid` int(11) unsigned NOT NULL,
  `name` text NOT NULL,
  `de:amtlicher_gemeindeschluessel` text NOT NULL,
  `gemeindename` text NOT NULL,
  `ref` text NOT NULL,
  `type` text NOT NULL,
  `status` enum('todo','changed','wont-fix') NOT NULL,
  `hint` tinytext NOT NULL,
  `left` float DEFAULT NULL,
  `bottom` float DEFAULT NULL,
  `right` float DEFAULT NULL,
  `top` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
