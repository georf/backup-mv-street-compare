<?php
function __autoload($class_name) {
  include str_replace("\\", "/", $class_name) . '.php';
}

use RestService\Server;
use MGVmedia\Street;

Server::create('/')
  ->addGetRoute('streets/missing-name/(\\d+)(/[^/]+)?', function ($osmId, $status = false) {
    $streets = Street::create()->hint('missing_name')->osmId($osmId);

    if ($status) {
      $status = preg_replace("|^/|", "", $status);
      if (Street::isStatus($status)) {
        $streets->status($status);
      } else {
        throw new Exception("bad parameter ".$status);
      }
    }
    return $streets->runAll();
  })
  ->addGetRoute('streets/([^/]+)(/[^/]+)?(/[^/]+)?', function ($bbox, $optional1 = false, $optional2 = false) {
    $streets = Street::create()->bbox(Street::checkBbox($bbox));

    foreach (array(preg_replace("|^/|", "", $optional1), preg_replace("|^/|", "" , $optional2)) as $opt) {
      if ($opt) {
        if (Street::isStatus($opt)) {
          $streets->status($opt);
        } elseif (Street::isHint($opt)) {
          $streets->hint($opt);
        } else {
          throw new Exception("bad parameter ".$opt);
        }
      }
    }
    return $streets->run();
  })
  ->addPostRoute('street/([0-9]+)', function($streetId, $status) {
    if (!Street::isStatus($status)) throw new Exception("bad parameter");

    return Street::create()->id($streetId)->setStatus($status);
  })
->run();
